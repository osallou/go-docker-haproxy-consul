# About

Files and method from http://sirile.github.io/2015/05/18/using-haproxy-and-consul-for-dynamic-service-discovery-on-docker.html

HAProxy load balancing from consul. Maps *web* tags from go-docker web servers and perform round-robin distribution to the servers up and running according to consul checks

# Sample consul startup (not for production)

    docker run --rm -p 8400:8400 -p 8500:8500  -p 8600:53/udp -h node1 gliderlabs/consul-server -server -advertise 131.254.17.40 -bootstrap -ui-dir /ui

# Setup

    curl -X PUT -d '1024' http://IP_OF_CONSUL:8500/v1/kv/service/haproxy/maxconn
    curl -X PUT -d '3600' http://IP_OF_CONSUL:8500/v1/kv/service/haproxy/timeouts

Optional to get haproxy stats for user credentials test/test (not for production)

    curl -X PUT -d 'dev' http://IP_OF_CONSUL:8500/v1/kv/service/haproxy/mode

# Test template

Replace 172.17.0.1 by IP address of consul

sudo docker run --dns 172.17.0.2 --rm haproxybalance consul-template -config=/tmp/haproxy.json -consul=consul.service.consul:8500 -dry -once

# Run

Replace 172.17.0.1 by IP address of consul

    docker run -d  -e SERVICE_NAME=web --name=godockerweb --dns 172.17.0.2 -p 80:80 -p 1936:1936 haproxybalance consul-template -config=/tmp/haproxy.json -consul=consul.service.consul:8500

# License

CC-BY-SA 3.0
